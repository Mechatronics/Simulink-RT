function y = dual2dec64(x)
% This function converts the an array of dual values x (64 bit) to a decimal
% value y

[n,m] = size(x);

if (m<64)
    x = [x zeros(n,64-m)];
end
    
y = zeros(n,1);
for k = 1:64
    y = x(:,k).*2^(k-1)+y;
end

end % end function
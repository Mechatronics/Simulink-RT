function y = dec2dual64(x)
% This function converts the decimal value x to an array of dual values y
% (64 bit)
[n,m] = size(x);

if(m>n)
    x = x.';
    n = m;
end 

y = zeros(n,64);
x = mod(x,2^64);

for k = 64:-1:1
    y(:,k) = double(x >= 2^(k-1));
    x = mod(x,2^(k-1));
end

end % end function
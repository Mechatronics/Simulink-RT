function y = dec2dual(x)
% This function converts the decimal value x to an array of dual values y
% (8 bit)
[n,m] = size(x);

if(m>n)
    x = x.';
    n = m;
end 

y = zeros(n,8);
x = mod(x,256);

for k = 8:-1:1
    y(:,k) = double(x >= 2^(k-1));
    x = mod(x,2^(k-1));
end

end % end function
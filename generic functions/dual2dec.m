function y = dual2dec(x)
% This function converts the an array of dual values x (8 bit) to a decimal
% value y

[n,m] = size(x);

if (m<8)
    x = [x zeros(n,8-m)];
end
    
y = zeros(n,1);
for k = 1:8
    y = x(:,k).*2^(k-1)+y;
end

end % end function